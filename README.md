# Labelprinter

This Project aims to connect Inventree to a Phomemo M110 label printer.
It does this, at the cost of the sanity of me, and everyone with knowledege of previous interations of the code.

## Setup
Inventree: Look up how to install addons, and put labelgecko.py where it gets found by inventree


FlaskFox: run the flake, with `nix run <path to flake>` or check the flake for dependencies, install them, and then  run the flaskfox.py file however you want.

Manual Printing: If all you really want is to just make the printer print, run the flake with `nix run <path to flake>#manual <path to pdf> <amount>`, or see the previous instruction and run the python manual.py file with the above arguments

If anything is going wrong, double check if the printer is actually connected.

## Architecture
labelgecko is the invetree addon that adds the printer support to inventree.
FlaskFox is the code that runs on a PI that is connected to the printer.
FlaskFox takes the label pdf from inventree, saves it, then converts it to send it to the printer.

A udev rule to allow access the printer is required to run this and can be found in the flaskfox directory, additionally you need to add your user to the lp group. You find the raw device at /dev/bus/usb/

## LORE
pdfduck is dead.
FlaskFox is guilty.
labelgecko is on the hunt.


It all began with bytefuck.py.


I got a script that was supposedly able to talk to that printer. It did, but it was... fucky.
After some agonizing time of trying to get python to just give me raw binary, I needed something that could convert the pdf of the label to a png.
I wanted to call that pdfuck.py but that would have been too much "fuck" so it became pdfduck.
Then labelgecko.py was made to be the inventree addon.
After that FlaskFox was created. FlaskFox decided to consume pdfduck. It hunted and killed pdfduck. Now labelgecko is on the hunt for flaskfox, trying to avenge pdfduck.

labeelgecko won. Flaskfox was defeated and brought in line with way more sensible code. pdfducks death was avenged. peace now exists in the realms.