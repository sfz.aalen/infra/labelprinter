{
  description = "labelgecko hunts flaskfox for it's crimes against PDFduck and sane code";

  outputs = { self, nixpkgs }:
    let
      system = "aarch64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };

      deps = with pkgs; [
        python311
        python311Packages.pillow
        python311Packages.flask
        python311Packages.pdf2image
        python311Packages.pyusb
        poppler_utils
      ];

      flaskfox = pkgs.python311Packages.buildPythonApplication {
        name = "flaskfox";
        src = ./flaskfox;
        doCheck = false;
        propagatedBuildInputs = deps;
      };
    in
    {
      devShells.${system}.default = pkgs.mkShell {
        packages = deps;
      };

      packages.${system} =
        {
          default = pkgs.writeShellScriptBin "flaskfox" ''
            ${flaskfox}/bin/flaskfox.py "''${@:1}"
          '';
          manual = pkgs.writeShellScriptBin "manual" ''
            ${flaskfox}/bin/manual.py "''${@:1}"
          '';

        };
    };
}

