#!/usr/bin/env python
from flask import Flask
from flask import request
import printpdf
import os

server = Flask(__name__)
user = "parry"
working_dir = f"/home/{user}/working_dir/"

@server.route('/print/<amount>', methods = ['POST'])
def handle_request(amount):
    label = request.files['label']
    label.save(working_dir + "./label.pdf")
    printpdf.print_pdfs(working_dir + "./label.pdf")
    return ['Done!\n']

#make sure the working directory exists
os.makedirs(working_dir, exist_ok=True)

#make the server run. fast. away.
server.run(host="0.0.0.0", port=56789, debug=True)
print("flaskfox has started to run. away. fast")
