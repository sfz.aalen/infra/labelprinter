#!/usr/bin/env python
from os.path import exists
import sys
import printpdf

label_location = sys.argv[1]
label_amount = int(sys.argv[2])
if(not exists(label_location)):
    print("label not found")
for i in range(0,label_amount):
    print(f'printing {i+1}/{label_amount}')
    printpdf.print_pdfs(label_location)
print('printing complete')