from PIL import Image
from pdf2image import convert_from_path
from os.path import exists
import usb.core
import usb.util

def print_pdfs(pdf_location:str):
    if not exists(pdf_location):
        raise ValueError(f'Cannot find file at {pdf_location}')
    print(f'printing: {pdf_location}')
    images = convert_from_path(pdf_location)
    print(f'extracted {len(images)} indiviual labels')
    for image in images:
        print_image(image)

def print_image(image):
    if image is None:
        raise ValueError('attempting to print None Image, aborting')
    image = image.convert(mode='L')
    image = image.resize((384,240))
    binary = convert_image_to_binary(image)
    print_binary(binary)
    
def convert_image_to_binary(image):
    buf = []
    buf += [0x1b, 0x40]
    buf += [0x1d, 0x76, 0x30, 0x0]

    width = image.width
    height = image.height

    assert(width == 384)
    assert(height == 240)

    width8 = width//8

    buf += [width8&0xff, width8>>8]
    buf += [height&0xff, height>>8]

    ibuf = [0]*height*width8
    for y in range(height) :
        for x in range(width) :
            if image.getpixel((x,y)) < 255/2:
                ibuf[x//8 + y*width8] |= (1<<(7-(x&7)))

    buf += ibuf
    return buf

def print_binary(binary):
    # first, find the printer
    device = usb.core.find(idVendor=0x0483, idProduct=0x5740)
    if device is None:
        raise ValueError('Could not find labelprinter')

    if device.is_kernel_driver_active(0):
        try:
            device.detach_kernel_driver(0)
            print("Kernel driver detached")
        except usb.core.USBError as e:
            sys.exit("Could not detach kernel driver: %s" % str(e))

    # then print
    device.write(1, bytes(binary))
    usb.util.dispose_resources(device)