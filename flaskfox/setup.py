#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name="flaskfox",
    version="1.0",
    packages=find_packages(),
    scripts=['flaskfox.py','printpdf.py','manual.py'],
)
