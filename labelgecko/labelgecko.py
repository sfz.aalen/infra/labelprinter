# -*- coding: utf-8 -*-
from plugin import InvenTreePlugin
from plugin.mixins import LabelPrintingMixin, SettingsMixin
import requests

class LabelGecko(LabelPrintingMixin, SettingsMixin, InvenTreePlugin):

    NAME = "LabelGecko"
    TITLE = "Label Gecko"
    DESCRIPTION = "Print to the label printer. If this decides to work, that is"
    VERSION = "0.0.1"
    AUTHOR = "Grauly"

    SETTINGS = {
        'IP_ADDRESS': {
            'name': 'FlaskFox IP Address',
            'description': 'The IP where the other half of this mess runs on.',
            'default': '10.12.42.77',
            'required': True
            },
        'PORT': {
            'name': 'FlaskFox IP Port',
            'description': 'The Port for the IP where FlaskFox is trying its best to run away from you.',
            'default': 56789,
            'required': True
            },
        'SERVICE_PATH': {
            'name': 'FlaskFox Service Path',
            'description': 'The service location where FlaskFox is hiding. From you. You monster.',
            'default': '/print/1',
            'required': True
            }
        }

    BLOCKING_PRINT = True

    def print_label(self, **kwargs):
        ip = self.get_setting('IP_ADDRESS')
        port = self.get_setting('PORT')
        path = self.get_setting('SERVICE_PATH')
        print(f'Attemping to upload label to: http://{ip}:{port}{path}')
        requests.post(f'http://{ip}:{port}{path}', files = {'label': kwargs['pdf_data']})
